import CourseCard from '../components/CourseCard';
//import coursesData from '../data/coursesData';
import { useState, useEffect, useContext } from 'react';



export default function Courses() {

	// state that will be used to store the courses retrieved from the database
	const [courses, setCourses] = useState([])


	// Checks to see if the mock data was captured
	//console.log(coursesData);
	//console.log(coursesData[0]);

	// The "map()" method loops through the individual course object in our array and return a component for each course
	// Everytime the map method loops through the data, it creates a "CourseCard" component and then passes the current element in our coursesData array using the "courseProp"
	// const courses = coursesData.map(course =>{
	// 	return (
	// 			//props
	// 		<CourseCard key={course.id} course={course}/>
	// 	)
	// })

	// Retrieves the courses from the database upon initial render of "courses" component
	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/courses/active`)
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setCourses(data.map(course =>{
				return(
				<CourseCard key={course._id} course={course}/>
				)
			}))
		})
	},[])


	// Props Drilling - allows us to pass information from one component to another using "props"
	// Curly braces {} are used for props to signify that we are providing/passing information
	return(
		<>
		{courses}
		</>
		)
}
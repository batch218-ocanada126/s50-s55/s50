//s52 activity

import { useState, useEffect, useContext } from 'react';

import { Form, Button } from 'react-bootstrap';
//import {useNavigate} from 'react-router-dom'; // to route 
import {Navigate} from 'react-router-dom';

import Swal from 'sweetalert2';

import UserContext from '../UserContext';

export default function Login() {

  // Allows us to consume/use the User Context Object and properties to use for user validation
  const {user, setUser} = useContext(UserContext);

  // State hooks to store the values of the input fields
	const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  //State to determine whether submit button is enabled or not
  const [isActive, setIsActive] = useState(true);

  // hook that returns a function that let us navigate to components
  //const navigate = useNavigate()

  	// Function to simulate redirection via from submission
  			//authenticate
    function loginUser(e){ //authenticate function
    	e.preventDefault()

      fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
          method: 'POST',
          headers: {
            'Content-Type' : 'application/json'
          },
          body: JSON.stringify({
              email: email,
              password: password
          })

      })
      .then(res => res.json())
      .then(data => {
        console.log(data)

          if(typeof data.access !== 'undefined') {
              localStorage.setItem('token', data.access);
              retrieveUserDetails(data.access);

              Swal.fire({
                  title: "Login Successful",
                  icon: "success",
                  text: "Welcome to Zuitt!"
              })
          } 
          else {
                  Swal.fire({
                  title: "Authentication Failed",
                  icon: "error",
                  text: "Please, check your login details and try again."
              })


          }
      });

    	// set the email if the authenticated/login user in the local storage
    	//localStorage.setItem('email', email);
      // sets the global user state to have properties obtained from local storage
      //setUser({email: localStorage.getItem('email')});

    	setEmail("");
    	setPassword("");
    	//navigate('/');

    	//alert(`${email} has been verified! Welcome back!`); //(`${email} has been verified! Welcome back!`)

    };
    // if login redirect to courses page
    const retrieveUserDetails = (token) => {
        fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
            headers: {
                Authorization: `Bearer ${token}`
            }
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);

            // Changes the global 'user' state to store the 'id' and the type "isAdmin" property of the user which will be used for validation accross the whole application
            setUser({
              id: data._id,
              isAdmin: data.isAdmin
            })
        })
    };


    // useEffect - is a piece of code that you want to execute.
    useEffect(() =>{
    	if(email !== "" && password !== ""){
    		setIsActive(true);
    	} else{
    		setIsActive(false);
    	}
    	//dependencies - everytime there has a changes(email,password) it will automatically execute the function useEffect()
    }, [email, password])


  return (

      (user.id !== null) ?
      <Navigate to ="/courses" />
      :
  					//binding
    <Form onSubmit={(e) => loginUser(e)}>
    <h1>Login</h1>
      <Form.Group className="mb-3" controlId="formBasicEmail">
        <Form.Label>Email address</Form.Label>
        <Form.Control 
        type="email" 
        placeholder="Enter email"
        //binding
        value={email}
        onChange={e => setEmail(e.target.value)}
        required />
        <Form.Text className="text-muted">
          We'll never share your email with anyone else.
        </Form.Text>
      </Form.Group>

      <Form.Group className="mb-3" controlId="formBasicPassword">
        <Form.Label>Password</Form.Label>
        <Form.Control 
        type="password"
        placeholder="Password"
        //binding
        value={password}
        onChange={e => setPassword(e.target.value)}
        required />
      </Form.Group>
      <Form.Group className="mb-3" controlId="formBasicCheckbox">
      </Form.Group>
      { isActive ? //ternary condition - condition ? true : false
                 <Button variant="success" type="submit">
                Login
                 </Button>
                 :
                  <Button variant="info" type="submit" disabled>
                Login
                 </Button>

                 }
    </Form>
  );
}


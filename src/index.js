import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';
// Importing the Bootstrap CSS
import 'bootstrap/dist/css/bootstrap.min.css'


const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
  );

// const name = "Chirstian James";
// // JSX - JavaScript XML
// // With JSX we can apply JS logic with HTML elements
// const element = <h1>Hello, {name}</h1>

// const user = {
//   firstName: "Jane",
//   lastName: "Smith"
// };

// function formatName(user){
//   return user.firstName + " " + user.lastName;
// };

// const element = <h1>Hello, {formatName(user)}</h1>


//root.render(element);

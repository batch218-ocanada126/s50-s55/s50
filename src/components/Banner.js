
import {Button, Col, Row} from 'react-bootstrap';
import { Link } from "react-router-dom";

/*export default function Banner({type}) {
  return (
    <Row>
    
        {
          type === "home" ? (
            <>
        <Col className="p-5">    
        <h1>Zuitt Coding Bootcamp</h1>
        <p>Opportunities for everyone, everywhere.</p>
        <Button variant="primary">Enroll now!</Button>
         </Col>
        </>
        ) : (
            <>

            <h1>Page Not Found</h1>

            <p>Go back to the <Link as={Link} to='/'>homepage.</Link></p>

            </>
          )
        }
     
    </Row>
  )
}*/

// refactoring Banner to become reuseable for Home.js and Error.js
export default function Banner({data}){

    const {title, content, destination, label} = data;

    return (

    <Row>
          <Col className="p-5">
           <h1>{title}</h1>
           <p>{content}</p>
           {/*<Link to={destination}>{label}</Link>*/}
           <Button variant="primary" as={Link} to={destination}>{label}</Button>
              
          </Col>  
    </Row>

)


}

